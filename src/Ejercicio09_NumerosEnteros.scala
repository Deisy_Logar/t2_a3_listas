import scala.collection.immutable.List


/*
			Ejercicio 9.
				
				Dada una lista de n�meros enteros escribir una funci�n que:
				
					a) Devuelva una lista con todos los que sean primos.
					b) Devuelva la sumatoria y el promedio de los valores.
					c) Devuelva una lista con el factorial de cada uno de esos n�meros.
			
*/


object NumerosEnteros {
  
  def numerosPrimos(): Unit = {
    // No pude pasar como par�metro una lista
  }
  
  def main(args: Array[String]): Unit = {
    
    println("\n\t\t *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* PALABRAS PAL�NDROMAS *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*")
    
    val listaNumeros = List(37, 1, 19, 2, 2, 3, 26, 4, 17, 5, 45, 6, 43, 7, 49, 8, 14, 9, 29, 10)
    
    println("\n" + listaNumeros)
    
  }
}