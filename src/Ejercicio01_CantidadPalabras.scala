

/*
			Ejercicio 1.
			
				Escriba un programa que permita crear una lista de palabras y que, a continuaci�n, pida una palabra
				y diga cu�ntas veces aparece esa palabra en la lista.
			
*/



import scala.io.StdIn._

object CantidadPalabras {
  
  def main(args: Array[String]): Unit = {
    
    println("\n\t\t *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* CANTIDAD DE PALABRAS *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*")
    
    val listaPalabras = List("LUZ", "SONRISA", "CINE", "PELICULA", "MUSICA", "INFINITO", "FAUNA", "NUBE", "PIANO", "CHOCOLATE",
                              "CINE", "PELICULA", "NUBE", "CHOCOLATE", "FAUNA", "PELICULA", "CHOCOLATE", "NUBE", "CHOCOLATE")
    
    println("\n\t ---------- Lista de Palabras ----------")
    
    println("\n" + listaPalabras)
    
    println("\n Ingrese una palabra: ")
    var palabra = readLine()
    
    palabra = palabra.toUpperCase()
    
    println("------> Tu palabra ingresada es: " + palabra)
    
    var aux: Int = 0
    var cantPalabras: Int = 0
    
    for (x <- listaPalabras) {
      
      if (listaPalabras(aux) == palabra) {
        cantPalabras += 1
      }
      aux += 1
    }
    println("\n La palabra '" + palabra + "' se repite " + cantPalabras + " veces.")
    
  }
}