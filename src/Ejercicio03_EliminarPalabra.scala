

/*
			Ejercicio 3.
			
				Escriba un programa que permita crear una lista de palabras y que, a continuación, pida una palabra
				y elimine esa palabra de la lista
			
*/



import scala.io.StdIn._

object EliminarPalabra {
  
  def main(args: Array[String]): Unit = {
    
    println("\n\t\t *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* ELIMINACIÓN DE PALABRAS *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*")
    
    val listaPalabras = List("LUZ", "SONRISA", "CINE", "PELICULA", "MUSICA", "INFINITO", "FAUNA", "NUBE", "PIANO", "CHOCOLATE",
                              "CINE", "PELICULA", "NUBE", "CHOCOLATE", "FAUNA", "PELICULA", "CHOCOLATE", "NUBE", "CHOCOLATE")
    
    println("\n\t ---------- Lista de Palabras ----------")
    
    println("\n" + listaPalabras)
    
    println("\n Ingrese una palabra: ")
    var palabra = readLine()
    
    palabra = palabra.toUpperCase()
    
    println("------> Tu palabra ingresada es: " + palabra)
    
    val palabras = listaPalabras.filterNot(palabraFil => palabraFil.equalsIgnoreCase(palabra))
    
    println("\n" + palabras)
    
  }
  
}