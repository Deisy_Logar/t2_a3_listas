

/*
			Ejercicio 7.
				
				Escribe una funci�n llamada "obtenerPalindroma" que busque todas las palabras pal�ndromas de
				una lista.
				
				Ejemplo de palabras inversas:
						* radar
						* oro
						* rajar
						* rallar
						* salas
						* somos
				
*/


object Palindromas {
  
  def main(args: Array[String]): Unit = {
    
    println("\n\t\t *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* PALABRAS PAL�NDROMAS *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*")
    
    val listaPalabras = List("RADAR", "ORO", "RAJAR", "RALLAR", "SALAS", "SOMOS", "ANA", "OSO", "ATARDECER", "SUE�O", "UNIR", "SORPRESA", "LLUVIA")
    
    println("\nLista de Palabras: \n" + listaPalabras)
    
    println("-----> Palabras que son pal�ndromas: ")
    
    for (x <- listaPalabras){
      
      if (x.equals(x.reverse)){
        print(x + ", ")
      }
    }
    
  }
  
}