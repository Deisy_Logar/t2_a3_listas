

/*
		Ejercicio 5.
		
			Escriba un programa que permita crear dos listas de palabras y que, a continuaci�n, escriba las
			siguientes listas (en las que no debe haber repeticiones):
			
				* Lista de palabras que aparecen en las dos listas.
				* Lista de palabras que aparecen en la primera lista, pero no en la segunda lista.
				* Lista de palabras que aparecen en la segunda lista, pero no en la primera lista.
				* Lista de palabras que aparecen en ambas listas.
			
			NOTA:
						Para evitar las repeticiones, el programa deber� empezar eliminando los elementos repetidos
						en cada lista.
			
*/


object ListaPalabras {
  
  def main(args: Array[String]): Unit = {
    
    println("\n\t\t *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* LISTADO DE PALABRAS *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*")
    
    val listaPalabras1 = List("LUZ", "SONRISA", "CINE", "PELICULA", "MUSICA", "INFINITO", "FAUNA", "NUBE", "PIANO", "CHOCOLATE")
    
    val listaPalabras2 = List("SONRISA", "CINE", "PELICULA", "MUSICA", "CHOCOLATE", "ATARDECER", "SUE�O", "UNIR", "SORPRESA", "LLUVIA",
                              "ESPIRAL", "AURA", "AMISTAD", "CAMINO", "MEMORIA", "PRIMAVERA")
    
    println("\nLista de Palabras 1: \n" + listaPalabras1)
    
    println("\nLista de Palabras 2: \n" + listaPalabras2)
    
    //--------------------------------------------------------------------------------------------------
    
    println("\n\n Intersecci�n de Palabras de las Listas")
    
    println("-----> INTERSECCI�N:")
    
    val listaInterseccion = listaPalabras1.intersect(listaPalabras2)
    
    println(listaInterseccion)
    
    // -------------------------------------------------------------------------------------------------
    
    println("\n\n Palabras que aparecen en la primera lista, pero no en la segunda lista: ")
    
    println("-----> Palabras Lista 1:")
    
    val listPalabras1 = listaPalabras1.diff(listaPalabras2)
    
    println(listPalabras1)
    
    // -------------------------------------------------------------------------------------------------
    
    println("\n\n Palabras que aparecen en la segunda lista, pero no en la primera lista: ")
    
    println("-----> Palabras Lista 2:")
    
    val listPalabras2 = listaPalabras2.diff(listaPalabras1)
    
    println(listPalabras2)
    
    // -------------------------------------------------------------------------------------------------
    
    println("\n\n Uni�n de palabras de las listas: ")
    
    println("-----> UNI�N:")
    
    val listaUnion = listaPalabras1.union(listaPalabras2)
    
    println(listaUnion)
    
  }
  
}